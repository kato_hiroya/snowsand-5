import { Loading } from './_loading.js';
import { Scroll } from './_scroll.js';
import { Sticky } from './_sticky.js';
import { ViewIn } from './_observer.js';
import { Slider } from './_slider.js';

window.PC = (!__DETECT__.device.any && __WW__ > tabletStyle);
window.mobile = (__DETECT__.device.any || __WW__ <= tabletStyle);
window.tablet = (__DETECT__.device.tablet || (__WW__ <= tabletStyle && __WW__ > mobileStyle));
window.phone = (__DETECT__.device.phone || __WW__ <= mobileStyle);

window.pageScroll = null;
window.stickyScroll = null;
window.viewin = null;
window.dragSlider = null;
window.loading = new Loading();

//=============================================================
//迷惑メール対策

const mLink = () => {
  const mAdr = 'hello' + '@' + 'companycoc' + '.' + 'com';
  const mTitle = 'スノーサンドについてのお問い合わせ';
  document.querySelectorAll('.js-m').forEach((el, i) => {
    el.setAttribute('href', 'm' + 'ailto' + ':' + mAdr + '?subject=' + mTitle);
  });
};

//=============================================================
// 画像ロード

window.loadImage = () => {

  const $images = document.querySelectorAll('.js-img');
  $images.forEach((el, i) => {

    const src1x = el.dataset.src;
    const src2x = el.dataset.src2x;
    const srcMb = el.dataset.srcmb;
    let src = getImageSrc(src1x, src2x, srcMb);
    if (src === null) return;
    const img = new Image();
    img.onload = e => {
      let div = document.createElement('div');
      div.classList.add('bg-img-body');
      div.style.backgroundImage = 'url(' + src + ')';
      el.innerHTML = '';
      el.insertBefore(div, el.firstChild);
      el.classList.add('img-loaded');
    };
    img.onerror = e => {
      if (src && window.__DETECT__.webp && src.indexOf('.webp')) {
        src = src.split('.webp')[0];
        img.src = src;
      }
    };
    img.src = src;

  });
};

//=============================================================
// 外部リンクに自動追加

class ExternalLink {
  static method() {
    const externalLinks = document.querySelectorAll('a[href^="http"]:not([href^="' + window.location.protocol + '//' + window.location.host + '"])');
    const externalIcon = document.createElement('span');
    for (let i = 0; i < externalLinks.length; i++) {
      externalLinks[i].classList.add('external');
      externalLinks[i].target = '_blank';
      externalLinks[i].rel = 'nooener noreferrer';
      externalLinks[i].insertAdjacentElement('beforeend', externalIcon);
    }
  }
};

//=============================================================
// 文字を分割してspanで囲む

const charWrap = () => {
  const target = document.querySelectorAll('.charwrap');
  target.forEach((tgt, i) => {

    const nodes = tgt.childNodes;
    let convert = [];

    for (let node of nodes) {
      if (node.nodeType == 3) {
        //テキストの場合
        let text = node.textContent;
        text.split('').forEach((v) => {
          if (v === ' ') {
            convert.push('<span class="e">' + v + '</span>');
          } else {
            convert.push('<span>' + v + '</span>');
          }
        });

      } else {
        //テキスト以外
        convert.push(node.outerHTML);
      }
    }
    tgt.innerHTML = convert.join('');

  });
};

//=============================================================
// hover時にクラス付与

const addHoverClass = () => {
  const target = document.querySelectorAll('a, .scroll-to, .js-hov');
  target.forEach((tgt, i) => {

    tgt.addEventListener('mouseenter', () => {

      if (!PC) return;

      tgt.classList.add('enter');
      setTimeout(() => {
        tgt.classList.remove('enter');
      }, 800);

    });
  });
};

//=============================================================
// INIT
//=============================================================

window.init = () => {

  let $scrollWindows = document.querySelectorAll('.scroll-window');
  $window = $scrollWindows[$scrollWindows.length - 1];

  scrollEvent();

  pageScroll = new Scroll();
  stickyScroll = new Sticky();
  viewin = new ViewIn();
  dragSlider = new Slider();

  loadImage();
  resisterEvents();

  mLink();
  charWrap();
  addHoverClass();
  ExternalLink.method();

};

