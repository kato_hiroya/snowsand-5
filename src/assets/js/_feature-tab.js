// <home> 特徴セクション (TAB)
//=============================================================

export class FeatureTab {

  constructor() {

    this.timer = null; // タブ切替発火までのタイマー
    this.flag = false; // アニメーション中かどうか
    this.isClicked = false; // タブクリック時切り分け
    this.currentNum = -1;
    this.ease = gsap.parseEase('0.365, 0.155, 0.100, 1.000');
    this.initialize();

    const $featureTabOption = document.querySelectorAll('.features-select [data-tab-option]');
    $featureTabOption.forEach((el, i) => {

      // クリックでタブ切替位置までスクロール
      el.addEventListener('click', (e) => {

        this.isClicked = true;

        let num = el.dataset.tabOption;
        let posRatio = 0;
        if (num === '1') {
          posRatio = 0;
        } else if (num === '2') {
          posRatio = .25;
        } else if (num === '3') {
          posRatio = .5;
        }

        const $parent = document.querySelector('.sticky-content-area[data-area="features"]');
        const rect = $parent.getBoundingClientRect();
        const offsetY = (rect.y + pageScroll.y) + ((rect.height + __WH__) * posRatio);
        gsap.set(window, {
          scrollTo: { y: offsetY, autoKill: true }
        });

      }, false);

    });

  }


  initialize(el, num) {
    // 初期表示:1つめのタブを表示

    this.currentNum = 0;

    gsap.set('.features-option .option-border', { scaleX: 0, });
    gsap.set('.features-option .option-subt', { height: 0, });
    const $img = document.querySelectorAll('.features-tab.active .feature-img .img1, .features-tab.active .feature-img .img2');
    const $elements = document.querySelectorAll('.features-tab.active .feature-img-s, .features-tab.active .feature-tab-title, .features-tab.active .feature-tab-body');
    gsap.set($img, { opacity: 0, });
    gsap.set($elements, { opacity: 0, });

  }

  update(el, num, delay = 200) {
    // スクロールでタブ切替

    if (this.currentNum === num || (!PC && __WW__ <= tabletStyle)) return;
    this.currentNum = num;

    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      // アニメーションスタート
      this.flag = true;
      this.tabIn(el, document.querySelector('.features-tab.tab-' + num));
    }, delay);

  }

  tabOut($option, $tabs) {
    // 現在のタブを非表示

    const num = $option.dataset.tabOption;

    gsap.to($option.querySelector('.option-border'), {
      ease: this.ease,
      duration: .4,
      scaleX: 0,
    });
    gsap.to($option.querySelector('.option-subt'), {
      ease: this.ease,
      duration: .6,
      height: 0,
    });

    const imgD1_out = '.feature-img.double .img1';
    const imgD2_out = '.feature-img.double .img2';
    const imgS1_out = '.feature-img.single .img1';
    const imgS2_out = '.feature-img.single .img2';
    const elements_out = '.feature-img-s, .feature-tab-title, .feature-tab-body';

    // chocorate
    if ($tabs.querySelectorAll(imgD1_out)) {
      gsap.to(imgD1_out, {
        ease: this.ease,
        duration: .2,
        y: -10,
        x: -10,
        rotation: -4,
        opacity: 0,
      });
    }
    if ($tabs.querySelectorAll(imgD2_out)) {
      gsap.to(imgD2_out, {
        ease: this.ease,
        duration: .2,
        y: -80,
        x: 10,
        rotation: 4,
        opacity: 0,
      });
    }

    // cookie, shape
    if ($tabs.querySelectorAll(imgS1_out)) {
      gsap.to(imgS1_out, {
        ease: this.ease,
        duration: .2,
        y: -20,
        x: 0,
        opacity: 0,
      });
    }
    if ($tabs.querySelectorAll(imgS2_out)) {
      gsap.to(imgS2_out, {
        ease: this.ease,
        duration: .2,
        y: -20,
        x: 0,
        opacity: 0,
      });
    }
    // all
    if ($tabs.querySelectorAll(elements_out)) {
      gsap.to(elements_out, {
        ease: this.ease,
        duration: .2,
        opacity: 0,
        onComplete: () => {
          $option.classList.remove('selected');
          $tabs.classList.remove('active');
        }
      });
    }

  }

  tabIn($option, $tab) {
    // 新しいタブを表示

    const num = $option.dataset.tabOption;
    $option.classList.add('selected');
    $tab.classList.add('active');

    gsap.fromTo($option.querySelector('.option-border'), {
      scaleX: 0,
    }, {
      ease: this.ease,
      duration: .6,
      scaleX: 1,
    });
    gsap.fromTo($option.querySelector('.option-subt'), {
      height: 0,
    }, {
      ease: this.ease,
      duration: .6,
      height: '2em',
    });

    const $options = document.querySelectorAll('.features-option-body');
    $options.forEach((el, i) => {
      if ($option !== el) {
        let $tabs = document.querySelector('.features-tab.tab-' + el.dataset.tabOption);
        this.tabOut(el, $tabs);
      }
    });

    const imgD1_in = '.feature-img.double .img1';
    const imgD2_in = '.feature-img.double .img2';
    const imgS1_in = '.feature-img.single .img1';
    const imgS2_in = '.feature-img.single .img2';
    const elements_in = '.feature-img-s, .feature-tab-title, .feature-tab-body';

    if (num == 1) {
      // chocorate

      if ($tab.querySelectorAll(imgD1_in)) {
        gsap.fromTo(imgD1_in, {
          y: 200,
          x: 50,
          rotation: -9,
          opacity: 0,
        }, {
          ease: this.ease,
          duration: .6,
          delay: .2,
          y: 50,
          x: 0,
          rotation: -5,
          opacity: 1,
        });
      }
      if ($tab.querySelectorAll(imgD2_in)) {
        gsap.fromTo(imgD2_in, {
          y: 100,
          x: -50,
          rotation: 0,
          opacity: 0,
        }, {
          ease: this.ease,
          duration: .6,
          delay: .25,
          y: -50,
          x: 0,
          rotation: 5,
          opacity: 1,
        });
      }

    } else if (num == 2) {
      // cookie

      if ($tab.querySelectorAll(imgS1_in)) {
        gsap.fromTo(imgS1_in, {
          y: 120,
          x: 5,
          rotation: -4,
          opacity: 0,
        }, {
          ease: this.ease,
          duration: .6,
          delay: .2,
          y: 0,
          x: 0,
          rotation: 0,
          opacity: 1,
        });
      }

    } else if (num == 3) {
      // shape

      if ($tab.querySelectorAll(imgS2_in)) {
        gsap.fromTo($tab.querySelectorAll(imgS2_in), {
          y: 120,
          x: -5,
          rotation: 4,
          opacity: 0,
        }, {
          ease: this.ease,
          duration: .6,
          delay: .25,
          y: 0,
          x: 0,
          rotation: 0,
          opacity: 1,
        });
      }

    }

    if ($tab.querySelectorAll(elements_in)) {
      gsap.fromTo($tab.querySelectorAll(elements_in), {
        y: 80,
        opacity: 0,
      }, {
        ease: this.ease,
        duration: .5,
        delay: .3,
        y: 0,
        opacity: 1,
        onComplete: () => {
          this.flag = false;
          this.isClicked = false;
        }
      });
    }
    //
  }

}


//=============================================================
